import { Component, Input, OnInit } from '@angular/core';
import { User } from '@app/modules/auth/models/user.interface';
import { Message } from '../../models/message.model';

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.css'],
})
export class MessageComponent implements OnInit {
  @Input() message!: Message;
  userId: string = localStorage.getItem('id_chat_app') as string;
  constructor() {}

  ngOnInit(): void {}
  compareUsers() {
    return Number(this.userId) === Number(this.message.user?.id) ? true : false;
  }
}
