import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Observable } from 'rxjs';
import { Room, RoomPaginate } from '../../models/rom.interface';
import { ChatService } from '../../services/chat.service';
import { BsModalService, BsModalRef, ModalOptions } from 'ngx-bootstrap/modal';
import { CreateRomComponent } from '../create-rom/create-rom.component';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css'],
})
export class SidebarComponent implements OnInit {
  rooms: Room[] = [];
  bsModalRef?: BsModalRef;
  username: string = localStorage.getItem("nestjs_chat_username") as string;
  @Output() selectRoom: EventEmitter<Room> = new EventEmitter<Room>();
  constructor(
    private chatService: ChatService,
    private modalService: BsModalService
  ) { }

  ngOnInit(): void {
    this.chatService.getMyRooms().subscribe({
      next: (resp) => {
        this.rooms = resp.items;
      },
      error: () => { }
    })


  }

  openModalWithComponent() {
    const initialState: ModalOptions = {
      initialState: {
        list: [
          'Open a modal with component',
          'Pass your data',
          'Do something else',
          '...',
        ],
        title: 'Modal with component',
        backdrop: true,
      },
    };
    this.bsModalRef = this.modalService.show(CreateRomComponent, initialState);
    this.bsModalRef.content.closeBtnName = 'Close';
  }
  onSelectRoom(room: Room) {
    this.selectRoom.emit(room);
  }
}
