import { Meta } from '@angular/platform-browser';
import { Room } from './rom.interface';
import { User } from '../../auth/models/user.interface';

export interface Message {
  id?: number;
  text: string;
  user?: User;
  room: Room;
  created_at?: Date;
  updated_at?: Date;
}

export interface MessagePaginate {
  items: Message[];
  meta: Meta;
}
