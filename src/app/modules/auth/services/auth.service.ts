import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LoginModel } from '../models/login.model';
import { Observable, tap } from 'rxjs';
import { map } from 'rxjs/operators';
import { LoginResponse } from '../models/loginResponse.interface';
import { environment } from '@env/environment';
@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor(private http: HttpClient) { }

  login(user: LoginModel): Observable<LoginResponse> {
    return this.http
      .post<LoginResponse>(`${environment.apiUrl}/users/login`, user)
      .pipe(
        tap((res: LoginResponse) =>
          localStorage.setItem('nestjs_chat_app', res.access_token)
        ),
        tap((res: LoginResponse) =>
          localStorage.setItem('nestjs_chat_username', res.username)
        ),
        tap((res: LoginResponse) => localStorage.setItem('id_chat_app', res.id))
        // tap(() =>
        //   // this.snackbar.open('Login Successfull', 'Close', {
        //   //   duration: 2000,
        //   //   horizontalPosition: 'right',
        //   //   verticalPosition: 'top',
        //   // })
        // )
      );
  }
}
