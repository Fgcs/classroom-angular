import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '@env/environment';
import { catchError, Observable, tap, throwError } from 'rxjs';
import { User } from '../models/user.interface';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  constructor(private http: HttpClient) {}
  findByUsername(username: string): Observable<User[]> {
    const headers = new HttpHeaders().set('InterceptorSkipHeader', 'skip');
    return this.http.get<User[]>(
      `${environment.apiUrl}/users/find-by-username?username=${username}`,
      { headers }
    );
  }

  create(user: User): Observable<User> {
    return this.http.post<User>(`${environment.apiUrl}/users`, user).pipe(
      catchError((e) => {
        return throwError(e);
      })
    );
  }
}
