import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { User } from '@app/modules/auth/models/user.interface';
import { UserService } from '@app/modules/auth/services/user.service';
import {
  debounceTime,
  distinctUntilChanged,
  Observable,
  switchMap,
  tap,
  Observer,
  of,
  noop,
} from 'rxjs';
import { map } from 'rxjs/operators';
import { isBs3 } from 'ngx-bootstrap/utils';
@Component({
  selector: 'app-select-user',
  templateUrl: './select-user.component.html',
  styleUrls: ['./select-user.component.css'],
})
export class SelectUserComponent implements OnInit {
  isBs3 = isBs3();
  selected?: string;
  @Input() users: User[] = [];
  @Output() addUser: EventEmitter<User> = new EventEmitter<User>();
  @Output() removeuser: EventEmitter<User> = new EventEmitter<User>();
  selectedUser!: User;
  stateCtrl = new FormControl();
  suggestions$?: Observable<User[]>;
  errorMessage?: string;
  myForm = new FormGroup({
    state: this.stateCtrl,
  });
  constructor(private userService: UserService) {}

  ngOnInit(): void {
    this.suggestions$ = new Observable(
      (observer: Observer<string | undefined>) => {
        observer.next(this.myForm.value.state);
      }
    ).pipe(
      switchMap((query: string) => {
        if (query) {
          return this.userService.findByUsername(query);
        }
        return of([]);
      })
    );
  }

  addUserToForm(user: User) {
    if (!this.verifyUserInList(user)) {
      this.addUser.emit(user);
      this.myForm.patchValue({ state: null });
    }
  }
  verifyUserInList(found: User) {
    let notFound = false;
    this.users.forEach((user) => {
      if (user.id === found.id) {
        notFound = true;
      }
    });
    return notFound;
  }
  removeUserFromForm(user: User) {
    this.removeuser.emit(user);
  }

  setSelectedUser(user: User) {
    this.selectedUser = user;
  }

  displayFn(user: User) {
    if (user) {
      return user.username;
    } else {
      return '';
    }
  }
}
