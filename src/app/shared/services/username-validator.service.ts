import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {
  AbstractControl,
  AsyncValidator,
  ValidationErrors,
} from '@angular/forms';
import { environment } from '@env/environment';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
@Injectable({
  providedIn: 'root',
})
export class UsernameValidatorService implements AsyncValidator {
  constructor(private http: HttpClient) {}
  validate(control: AbstractControl): Observable<ValidationErrors | null> {
    const username = control.value;
    const headers = new HttpHeaders().set('InterceptorSkipHeader', 'skip');
    return this.http
      .get<any>(
        `${environment.apiUrl}/users/find-by-username?username=${username}`,
        { headers }
      )
      .pipe(
        map((resp) => {
          return resp.lenght ? { usernameFound: true } : null;
        })
      );
  }
}
