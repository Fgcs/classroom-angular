import { Component, OnInit } from '@angular/core';
import { Room } from '../../models/rom.interface';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css'],
})
export class MainComponent implements OnInit {
  selectRoom!: Room;
  constructor() {}

  ngOnInit(): void {}
  onSelectRoom(room: Room) {
    this.selectRoom = room;
  }
}
