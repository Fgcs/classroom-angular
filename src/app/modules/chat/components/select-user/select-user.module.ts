import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SelectUserComponent } from './select-user.component';
import { TypeaheadModule } from 'ngx-bootstrap/typeahead';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
@NgModule({
  declarations: [SelectUserComponent],
  imports: [
    CommonModule,
    TypeaheadModule.forRoot(),
    FormsModule,
    ReactiveFormsModule,
  ],
  exports: [SelectUserComponent],
})
export class SelectUserModule {}
