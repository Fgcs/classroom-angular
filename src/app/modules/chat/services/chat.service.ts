import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Message, MessagePaginate } from '../models/message.model';
import { Room, RoomPaginate } from '../models/rom.interface';
import { CustomSocket } from '../socket/custom-socket';

@Injectable({
  providedIn: 'root',
})
export class ChatService {
  constructor(private socket: CustomSocket) {}

  getAddedMessage(): Observable<Message> {
    return this.socket.fromEvent<Message>('messageAdded');
  }

  sendMessage(message: Message) {
    this.socket.emit('addMessage', message);
  }

  joinRoom(room: Room) {
    this.socket.emit('joinRoom', room);
  }

  leaveRoom(room: Room) {
    this.socket.emit('leaveRoom', room);
  }

  getMessages(): Observable<MessagePaginate> {
    return this.socket.fromEvent<MessagePaginate>('messages');
  }

  getMyRooms(): Observable<RoomPaginate> {
    return this.socket.fromEvent<RoomPaginate>('rooms');
  }

  emitPaginateRooms(limit: number, page: number) {
    this.socket.emit('paginateRooms', { limit, page });
  }

  createRoom(room: Room) {
    this.socket.emit('createRoom', room);
  }
}
