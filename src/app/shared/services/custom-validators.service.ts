import { Injectable } from '@angular/core';
import { AbstractControl, ValidationErrors } from '@angular/forms';

@Injectable({
  providedIn: 'root',
})
export class CustomValidatorsService {
  constructor() {}
  matchControls(control: string, control2: string) {
    return (formGroup: AbstractControl): ValidationErrors | null => {
      const password = formGroup.get(control)?.value;
      const passwordConfirm = formGroup.get(control2)?.value;

      if (password !== passwordConfirm) {
        formGroup.get(control2)?.setErrors({ passwordsNotMatching: true });
        return { passwordsNotMatching: true };
      }

      return null;
    };
  }
}
