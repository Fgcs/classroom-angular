import {
  AfterViewInit,
  Component,
  ElementRef,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  SimpleChanges,
  ViewChild,
} from '@angular/core';
import { Observable, combineLatest } from 'rxjs';
import { MessagePaginate } from '../../models/message.model';
import { Room } from '../../models/rom.interface';
import { ChatService } from '../../services/chat.service';
import { map, startWith, tap } from 'rxjs/operators';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
@Component({
  selector: 'app-room',
  templateUrl: './room.component.html',
  styleUrls: ['./room.component.css'],
})
export class RoomComponent
  implements OnInit, OnChanges, OnDestroy, AfterViewInit
{
  @Input() chatRoom!: Room;
  @ViewChild('messages') private messagesScroller!: ElementRef;

  messagesPaginate$: Observable<MessagePaginate> = combineLatest([
    this.chatService.getMessages(),
    this.chatService.getAddedMessage().pipe(startWith(null)),
  ]).pipe(
    map(([messagePaginate, message]) => {
      if (
        message &&
        message.room.id === this.chatRoom.id &&
        !messagePaginate.items.some((m) => m.id === message.id)
      ) {
        messagePaginate.items.push(message);
      }
      console.log(messagePaginate);

      const items = messagePaginate.items.sort(
        (a, b) =>
          new Date(a.created_at as Date).getTime() -
          new Date(b.created_at as Date).getTime()
      );
      messagePaginate.items = items;
      return messagePaginate;
    }),
    tap(() => this.scrollToBottom())
  );
  messageForm: FormGroup = this.fb.group({
    text: ['', Validators.required],
  });
  sumbitted: boolean = false;
  constructor(private chatService: ChatService, private fb: FormBuilder) {}

  ngOnInit(): void {}
  ngOnChanges(changes: SimpleChanges) {
    this.chatService.leaveRoom(changes['chatRoom'].previousValue);
    if (this.chatRoom) {
      this.chatService.joinRoom(this.chatRoom);
    }
  }

  ngAfterViewInit() {
    if (this.messagesScroller) {
      this.scrollToBottom();
    }
  }

  ngOnDestroy() {
    this.chatService.leaveRoom(this.chatRoom);
  }
  scrollToBottom(): void {
    try {
      setTimeout(() => {
        this.messagesScroller.nativeElement.scrollTop =
          this.messagesScroller.nativeElement.scrollHeight;
      }, 1);
    } catch {}
  }
  get f() {
    return this.messageForm.controls;
  }
  sendMessage() {
    this.sumbitted = true;
    if (this.messageForm.valid) {
      this.chatService.sendMessage({
        text: this.messageForm.value.text,
        room: this.chatRoom,
      });
      this.messageForm.reset();
      this.sumbitted = false;
    }
  }
}
