import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ChatRoutingModule } from './chat-routing.module';
import { MainComponent } from './components/main/main.component';
import { SidebarModule } from './components/sidebar/sidebar.module';
import { RoomComponent } from './components/room/room.component';
import { CreateRomComponent } from './components/create-rom/create-rom.component';
import { SelectUserComponent } from './components/select-user/select-user.component';
import { SelectUserModule } from './components/select-user/select-user.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MessageModule } from './components/message/message.module';

@NgModule({
  declarations: [MainComponent, RoomComponent, CreateRomComponent],
  imports: [
    CommonModule,
    ChatRoutingModule,
    SidebarModule,
    SelectUserModule,
    ReactiveFormsModule,
    FormsModule,
    MessageModule,
  ],
})
export class ChatModule {}
