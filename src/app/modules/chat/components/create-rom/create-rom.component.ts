import { Component, OnInit } from '@angular/core';
import {
  FormArray,
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { User } from '@app/modules/auth/models/user.interface';
import { BsModalService } from 'ngx-bootstrap/modal';
import { ChatService } from '../../services/chat.service';

@Component({
  selector: 'app-create-rom',
  templateUrl: './create-rom.component.html',
  styleUrls: ['./create-rom.component.css'],
})
export class CreateRomComponent implements OnInit {
  roomForm: FormGroup = this.fb.group({
    name: ['', Validators.required],
    description: ['', Validators.required],
    users: this.fb.array([]),
  });
  submitted: boolean = false;

  constructor(
    private fb: FormBuilder,
    private chatService: ChatService,
    private modalService: BsModalService
  ) {}

  ngOnInit(): void {}
  initUser(user: User) {
    return new FormControl({
      id: user.id,
      username: user.username,
      email: user.email,
    });
  }
  get usersArr() {
    return this.roomForm.get('users') as FormArray;
  }
  get f() {
    return this.roomForm.controls;
  }

  addUser(userFormControl: FormControl) {
    this.usersArr.push(
      this.fb.control(userFormControl.value, Validators.required)
    );
  }

  removeUser(userId?: string) {
    this.usersArr.removeAt(
      this.usersArr.value.findIndex((user: User) => user.id === userId)
    );
  }
  onSumbit() {
    this.submitted = true;
    if (this.roomForm.valid) {
      this.chatService.createRoom(this.roomForm.getRawValue());
      this.modalService.hide();
    }
  }
}
