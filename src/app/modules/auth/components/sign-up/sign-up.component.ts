import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { Router } from '@angular/router';
import { CustomValidatorsService } from '@app/shared/services/custom-validators.service';
import { EmailValidatorService } from '@app/shared/services/email-validator.service';
import { UsernameValidatorService } from '@app/shared/services/username-validator.service';
import { tap } from 'rxjs';
import { CustomValidators } from '../../helpers/custom-validators';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css'],
})
export class SignUpComponent implements OnInit {
  submitted: Boolean = false;
  signUpForm: FormGroup = this.fb.group(
    {
      email: [
        '',
        [
          Validators.required,
          Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$'),
        ],
        [this.emailValidator],
      ],
      username: ['', Validators.required, [this.usernameValidator]],
      password: ['', Validators.required],
      passwordConfirm: ['', Validators.required],
    },
    {
      validators: [
        this.customValidators.matchControls('password', 'passwordConfirm'),
      ],
    }
  );
  constructor(
    private fb: FormBuilder,
    private userService: UserService,
    private router: Router,
    private customValidators: CustomValidatorsService,
    private emailValidator: EmailValidatorService,
    private usernameValidator: UsernameValidatorService
  ) {}

  ngOnInit(): void {}

  onSubmit() {
    this.submitted = true;
    if (this.signUpForm.invalid) return;
    this.userService
      .create({
        email: this.signUpForm.value.email,
        password: this.signUpForm.value.password,
        username: this.signUpForm.value.username,
      })
      .pipe(tap(() => this.router.navigate(['/auth/login'])))
      .subscribe();
  }
  get f() {
    return this.signUpForm.controls;
  }
}
