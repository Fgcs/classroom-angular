export const environment = {
  production: true,
  apiUrl: 'https://classroom-ysm4.onrender.com/api',
  socketApi: 'https://classroom-ysm4.onrender.com',
};
